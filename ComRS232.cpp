#include "ComRS232.h"

ComRS232::ComRS232(const ComRS232Config& configStruct)
	: m_cComConfig(configStruct)
{
	open(READWRITE);
}

ComRS232::~ComRS232()
{
	close();
}

bool		ComRS232::open ( ComRS232::OpenMode mode ) throw()
{
	DCB dcb = {sizeof(DCB)};

	dcb.BaudRate 		= m_cComConfig.bpsRate;
	dcb.fBinary 		= TRUE;
	dcb.fParity 		= (m_cComConfig.parity == ComRS232Config::PAR_NONE ? FALSE : TRUE);
	dcb.fOutxCtsFlow 	= (m_cComConfig.flowControl == ComRS232Config::FLOW_CTRL_HARDWARE ? TRUE : FALSE);
	dcb.fOutxDsrFlow 	= (m_cComConfig.flowControl == ComRS232Config::FLOW_CTRL_HARDWARE ? TRUE : FALSE);
	dcb.fDtrControl 	= (m_cComConfig.flowControl == ComRS232Config::FLOW_CTRL_HARDWARE ? DTR_CONTROL_ENABLE : DTR_CONTROL_DISABLE);
	dcb.fDsrSensitivity = FALSE;
	dcb.fTXContinueOnXoff = TRUE;
	dcb.fOutX       	= (m_cComConfig.flowControl == ComRS232Config::FLOW_CTRL_SOFTWARE ? TRUE : FALSE);
	dcb.fInX        	= (m_cComConfig.flowControl == ComRS232Config::FLOW_CTRL_SOFTWARE ? TRUE : FALSE);
	dcb.fErrorChar  	= FALSE;
	dcb.fNull       	= FALSE;
	dcb.fRtsControl  	= (m_cComConfig.flowControl == ComRS232Config::FLOW_CTRL_HARDWARE ? RTS_CONTROL_TOGGLE : RTS_CONTROL_DISABLE);
	dcb.fAbortOnError 	= FALSE;
	dcb.fDummy2     	= 0;
	dcb.wReserved       = 0;
	dcb.XonLim          = 0;
	dcb.XoffLim         = 0;
	dcb.ByteSize        = (BYTE)m_cComConfig.dataBits;
	dcb.Parity          = m_cComConfig.parity;
	dcb.StopBits        = (BYTE)m_cComConfig.stopBits;
	dcb.XonChar         = m_cComConfig.xOnChar;
	dcb.XoffChar        = m_cComConfig.xOffChar;
	dcb.ErrorChar       = 0;
	dcb.EofChar         = m_cComConfig.eof;
	dcb.EvtChar         = 0;

	COMMTIMEOUTS timeouts = {
		m_cComConfig.readTimeout, 0, 100, m_cComConfig.writeTimeout, 100
	};

	COMMCONFIG comConfig = 	{
	    sizeof (COMMCONFIG),
		1,
		0,
		dcb,
		PST_RS232,
		0,
		0
	};

	char buff[12];
	sprintf(buff, "\\\\.\\COM%d", m_cComConfig.portNumber);
	comPortFileHandle = CreateFileA(buff, 
		GENERIC_READ|GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		0,
		NULL);
	
	if (isOpen())
	{
		//SetupComm(comPortFileHandle, 1, 1);
		SetCommConfig(comPortFileHandle, &comConfig, comConfig.dwSize);
		SetCommTimeouts(comPortFileHandle, &timeouts);

		flush();

		return true;
	}

	return false;
}
void		ComRS232::close () throw()
{
	if (comPortFileHandle != INVALID_HANDLE_VALUE)
	{
		CloseHandle(comPortFileHandle);
		comPortFileHandle = INVALID_HANDLE_VALUE;
	}
}

bool		ComRS232::reset ()
{	
	close();
	return open();
}

bool		ComRS232::isOpen() const
{	
	return comPortFileHandle != INVALID_HANDLE_VALUE;
}

std::string		ComRS232::errorString () const
{
	char* msg;
	DWORD res = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, 
					GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&msg, 0, NULL);
	
	if (!res)
		return "Unknown error";
	
	return msg;
}

bool		ComRS232::setBlocking(bool blocking, size_t time )
{
	return false;
}

bool		ComRS232::readChar ( char& c )
{
	if (!isOpen())
		return false;

	DWORD realsize = 0;
	BOOL ok = ReadFile(comPortFileHandle, &c, 1, &realsize, NULL);

	return ok == TRUE && realsize != 0;
}

ComRS232::ByteArray	ComRS232::readAll()
{
	ComRS232::ByteArray res;

	char buffer;
	while (readChar(buffer))
		res.push_back(buffer);

	return res;
}

int		ComRS232::read (ComRS232::ByteArray& byteArray, size_t count)
{
	char buffer;
	while (readChar(buffer) && count--)
		byteArray.push_back(buffer);

	return byteArray.size();
}

ComRS232::ByteArray	ComRS232::readLine (size_t count)
{
	ComRS232::ByteArray res;
	
	char buffer;
	while (readChar(buffer) && count-- && buffer != '\n')
		res.push_back(buffer);

	return res;
}

int		ComRS232::write ( const ComRS232::ByteArray& byteArray )
{
	if (!isOpen())
	{
		return false; 
	}

	DWORD fullRealSize = 0;

	while(fullRealSize < byteArray.size())	
	{
		DWORD realSize = 0;

		BOOL ok = WriteFile(comPortFileHandle, byteArray.data()+fullRealSize, byteArray.size()-fullRealSize, &realSize, 0);

		fullRealSize += realSize;

		if (ok == FALSE)
		{
			DWORD err = GetLastError();
			break;
		}

		if (!realSize)
		{
			break;
		}
	}

	return fullRealSize;
}

int		ComRS232::writeRead(const ComRS232::ByteArray& toWrite, ComRS232::ByteArray& forRead)
{
	write(toWrite);
	return read(forRead);
}

int		ComRS232::writeRead( ComRS232::ByteArray& writeAndReadBuffer)
{
	write(writeAndReadBuffer);

	writeAndReadBuffer.clear();

	return read(writeAndReadBuffer);
}

bool ComRS232::flush()
{
	BOOL ok =  PurgeComm(comPortFileHandle, PURGE_RXABORT);
		 ok &= PurgeComm(comPortFileHandle, PURGE_TXABORT);
		 ok &= PurgeComm(comPortFileHandle, PURGE_RXCLEAR);
		 ok &= PurgeComm(comPortFileHandle, PURGE_TXCLEAR);
	
	return ok != 0;
}

ComRS232Config ComRS232::getConfig() const
{
	return m_cComConfig;
}
