#include <ComRS232Config.h>

#include <map>
#include <algorithm>

#define DEFAULT_BUFFERSIZE 1 
#define DEFAULT_TIMEOUT 

const unsigned char ComRS232Config::DEFAULT_XON = 0x11;
const unsigned char ComRS232Config::DEFAULT_XOFF = 0x13;
const unsigned char ComRS232Config::DEFAULT_EOF = 0;
const int			ComRS232Config::INFINITE_TIMEOUT = -1;

ComRS232Config::ComRS232Config()
	:	dataBits(ComRS232Config::DATA_BITS_EIGHT), 
		parity(ComRS232Config::PAR_NONE), 
		startBits(ComRS232Config::START_ONE), 
		stopBits(ComRS232Config::STOP_ONE), 
		flowControl(ComRS232Config::FLOW_CTRL_HARDWARE), 
		xOnChar(ComRS232Config::DEFAULT_XON), 
		xOffChar(ComRS232Config::DEFAULT_XOFF),
		useEOF(true), 
		eof(ComRS232Config::DEFAULT_EOF), 
		bufferSize(DEFAULT_BUFFERSIZE), 
		readTimeout(ComRS232Config::INFINITE_TIMEOUT),
		writeTimeout(ComRS232Config::INFINITE_TIMEOUT)
{}
