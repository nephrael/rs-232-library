#pragma once

#include <Windows.h>
#include <string>
#include <vector>

class ComRS232Config
{
public:
	typedef int BPSRate;

	enum DataBits
	{
		DATA_BITS_FIVE = 5,
		DATA_BITS_SIX = 6,
		DATA_BITS_SEVEN = 7,
		DATA_BITS_EIGHT = 8
	};

	enum Parity
	{
		PAR_NONE = NOPARITY,
		PAR_ODD = ODDPARITY,
		PAR_EVEN = EVENPARITY,
		PAR_MARK = MARKPARITY,
		PAR_SPACE = SPACEPARITY
	};

	enum StartBits
	{
		START_ONE = ONESTOPBIT,
		START_ONE5 = ONE5STOPBITS,
		START_TWO = TWOSTOPBITS
	};

	enum StopBits
	{
		STOP_ONE = ONESTOPBIT,
		STOP_ONE5 = ONE5STOPBITS,
		STOP_TWO = TWOSTOPBITS
	};

	enum FlowControl
	{
		FLOW_CTRL_NONE,
		FLOW_CTRL_HARDWARE,
		FLOW_CTRL_SOFTWARE
	};

	static const unsigned char	DEFAULT_XON;
	static const unsigned char	DEFAULT_XOFF;
	static const unsigned char	DEFAULT_EOF;
	static const int			INFINITE_TIMEOUT;

public:
	unsigned		portNumber;
	BPSRate			bpsRate;
	DataBits		dataBits;
	unsigned char	parity;
	StartBits		startBits;
	StopBits		stopBits;
	FlowControl		flowControl;
	unsigned char	xOnChar;
	unsigned char	xOffChar;
	bool			useEOF;
	unsigned char	eof;
	int				bufferSize;
	int				readTimeout;
	int				writeTimeout;

	ComRS232Config();
};

