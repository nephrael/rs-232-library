#pragma once

#include <ComRS232Config.h>

class ComRS232
{
	ComRS232Config m_cComConfig;
	HANDLE comPortFileHandle;

	bool m_bIsOpen;
public:
	typedef unsigned char Byte;
	typedef std::vector<Byte> ByteArray;
	enum OpenMode
	{
		READ,
		WRITE,
		READWRITE,
		APPEND
	};
public:
	
	ComRS232(const ComRS232Config& configStruct);
	~ComRS232();

	ComRS232Config		getConfig() const;

	bool		open ( OpenMode mode = READWRITE ) throw();
	void		close () throw();
	bool		reset ();

	bool		isOpen () const;
	std::string	errorString () const;

	bool		setBlocking(bool blocking, size_t time = 500 );
	bool		flush();

	bool		readChar ( char& c );
	ByteArray	readAll();
	int			read (ByteArray& byteArray, size_t count = (size_t)-1);
	ByteArray	readLine (size_t count = (size_t)-1);

	int			write ( const ByteArray& byteArray);

	int			writeRead(const ByteArray& toWrite, ByteArray& forRead);
	int			writeRead(ByteArray& writeAndReadBuffer);
};

